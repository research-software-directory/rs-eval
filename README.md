<!--
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
SPDX-FileCopyrightText: 2023 Jonas Rimatzki (UP, GFZ) <rimatzkijo@yahoo.de; rimatzki@uni-potsdam.de; jr@gfz-potsdam.de>
SPDX-FileCopyrightText: 2023 Universität Potsdam

SPDX-License-Identifier: CC-BY-4.0
-->

# RS-Eval | Research Software Evaluator

## Description

RS-Eval is a proof-of-concept API framework build in python using docker. It evaluates research software from public git repositories in their compliance with the FAIR principles for research software (FAIR4RS) in the form of a survey implemented as part of this project. It was build for a master thesis in computational sciences.

## Installation & Usage

The setup us done via docker. To build the containers just use docker-compose. Same goes for setup.

The API endpoints are documented in german in the api-doku.pdf. Adjustments to the configuration can be made in the .env file or in the docker-compose.yml directly. Variables are named accordingly. Standard ports are incemented by 1, as this is planned as an ad-on to existing software repositories which might already use the standard ports.

## Support

As this is published using the Potsdam Universities Git I cannot promise indefinite access and support, as I myself might not be able to access this once I graduated.

## Contributing

Contribution is welcome in every shape or form. This is a loose framework and new tools for analysis can be easily added using the same methods as used for FAIR-ST.

## License

According to scientific practice the sourcecode is licensed under the EUPL-1.2 FOSS license.
Configuration files are licensed CC0-1.0.
Everything else is licensed under CC-BY-4.0.

## Project status

For now this project is on hold, as I got to focus on my future career paths and defending/writing my thesis.

## Origin

This project was part of the Master Thesis "Untersuchung automatischer Verfahren zur Verbesserung der Qualität und Reproduzierbarkeit von Forschungssoftware" at Universität Potsdam by Jonas Rimatzki.