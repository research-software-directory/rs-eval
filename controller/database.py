# SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2023 Jonas Rimatzki (UP, GFZ) <rimatzkijo@yahoo.de; rimatzki@uni-potsdam.de; jr@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2023 Universität Potsdam
# SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
#
# SPDX-License-Identifier: EUPL-1.2

import os, uuid, enum

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column
from sqlalchemy import String, Float, Enum, ARRAY, Integer, BOOLEAN
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
import uuid

# DB-Config
host = os.environ.get("POSTG_CONTAINER_NAME")
host = "localhost" if host is None else host
db = os.environ.get("POSTGRES_DB")
db = "db" if db is None else db
port = os.environ.get("POSTGRES_PORT")
port = "5433" if port is None else port
user = os.environ.get("POSTGRES_USER")
user = "user" if user is None else user
password = os.environ.get("POSTGRES_PASSWORD")
password = "pwd" if password is None else password

engine = create_engine(
    "postgresql+psycopg2://"
    + user
    + ":"
    + password
    + "@"
    + host
    + ":"
    + port
    + "/"
    + db
)
Base = declarative_base()
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def build():
    session = Session()
    Base.metadata.create_all(bind=engine)
    session.commit()
    session.close()


class Survey(Base):
    __tablename__ = "Survey"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = Column("title", String, nullable=False)
    version = Column("version", Integer)
    description = Column("description", String)
    hostname = Column("hostname", String)
    ip = Column("ip", String)
    port = Column("port", Integer)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return self.title


class Chapter(Base):
    __tablename__ = "Chapter"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    survey_id = Column(UUID(as_uuid=True), ForeignKey("Survey.id"), nullable=False)
    survey = relationship("Survey")
    title = Column("title", String, nullable=False)
    sort_no = Column("sort_no", Integer, nullable=False)
    description = Column("description", String)
    min_score = Column("min_score", Float)
    opt_score = Column("opt_score", Float)

    def __init__(self, surveyID, title, sort_no):
        self.survey_id = surveyID
        self.title = title
        self.sort_no = sort_no

    def __repr__(self):
        return self.title


class Question(Base):
    __tablename__ = "Question"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    chapter_id = Column(UUID(as_uuid=True), ForeignKey("Chapter.id"), nullable=False)
    chapter = relationship("Chapter")
    title = Column("title", String, nullable=False)
    description = Column("description", String)
    sort_no = Column("sort_no", Integer, nullable=False)
    weight = Column("weight", Float, nullable=False)
    answers = Column("answers", ARRAY(String), nullable=False)
    hints = Column("hints", ARRAY(String), nullable=False)

    def __init__(self, chapterID, title, sort_no, weight, answers, hints):
        self.chapter_id = chapterID
        self.title = title
        self.sort_no = sort_no
        self.weight = weight
        self.answers = answers
        self.hints = hints

    def __repr__(self):
        return self.text


class Software(Base):
    __tablename__ = "Software"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = Column("title", String, nullable=False)
    url = Column("url", String)

    def __init__(self, title, url):
        self.title = title
        self.url = url

    def __repr__(self):
        return self.title


class Inspection(Base):
    __tablename__ = "Inspection"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    software_id = Column(UUID(as_uuid=True), ForeignKey("Software.id"), nullable=False)
    software = relationship("Software")
    survey_id = Column(UUID(as_uuid=True), ForeignKey("Survey.id"), nullable=False)
    survey = relationship("Survey")

    def __init__(self, softwareID, surveyID):
        self.software_id = softwareID
        self.survey_id = surveyID

    def __repr__(self):
        return self.Survey.Title + " of " + self.Software.Title


class ChapterScore(Base):
    __tablename__ = "ChapterScore"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    inspection_id = Column(
        UUID(as_uuid=True), ForeignKey("Inspection.id"), nullable=False
    )
    inspection = relationship("Inspection")
    chapter_id = Column(UUID(as_uuid=True), ForeignKey("Chapter.id"), nullable=False)
    chapter = relationship("Chapter")
    answers = relationship("Answer", backref="ChapterScore")

    def __init__(self, inspectionID, chapterID):
        self.inspection_id = inspectionID
        self.chapter_id = chapterID

    def __repr__(self):
        return (
            self.inspection.survey.title
            + " >> "
            + self.chapter.title
            + " of "
            + self.inspection.software.title
        )


class Answer(Base):
    __tablename__ = "Answer"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    chapterscore_id = Column(
        UUID(as_uuid=True), ForeignKey("ChapterScore.id"), nullable=False
    )
    question_id = Column(UUID(as_uuid=True), ForeignKey("Question.id"), nullable=False)
    question = relationship("Question")
    survey_score = Column("survey_score", ARRAY(BOOLEAN))
    tool_score = Column("tool_score", ARRAY(BOOLEAN))

    def __init__(self, chapterScoreID, questionID):
        self.chapterscore_id = chapterScoreID
        self.question_id = questionID

    def __repr__(self):
        return self.question.title


class Status(enum.Enum):
    open = 0
    in_progress = 1
    done = 2
    error = 3


class Job(Base):
    __tablename__ = "Job"

    id = Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    software_id = Column(UUID(as_uuid=True), ForeignKey("Software.id"), nullable=False)
    survey_id = Column(UUID(as_uuid=True), ForeignKey("Survey.id"), nullable=False)
    status = Column("status", Enum(Status))

    def __init__(self, softwareID, surveyID):
        self.software_id = softwareID
        self.survey_id = surveyID
