# SPDX-FileCopyrightText: 2023 - 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2023 Jonas Rimatzki (UP, GFZ) <rimatzkijo@yahoo.de; rimatzki@uni-potsdam.de; jr@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2023 Universität Potsdam
# SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import os, requests

import database
from database import Survey, Chapter, Question, Software, Job, Status

from fastapi import FastAPI, Request
import uvicorn
import json
import debugpy

logging.basicConfig(level=logging.DEBUG)

database.build()
app = FastAPI()


@app.post("/register")
async def register(request: Request):
    """Register a new survey using a POST request.

    When the request is processed successfully, a new survey is registered in the
    database, including the associated chapters and questions.

    Args:
        request (fastapi.Request): The FastAPI request object.

    Returns:
        int: The ID of the newly registered survey.

    Raises:
        HTTPException: Returns status code 500 upon errors.

    Payload attributes:
        title (str): Survey title.
        hostname (str): Hostname of the survey.
        version (str): Survey version.
        description (str, optional): Survey description.
        port (int): Port for survey.
        chapter ([object]): The list of chapters of the survey, see below.

    Chapter attributes:
        title (str): Chapter title.
        description (str, optional): Description of the chapter.
        min_score (float, optional): Minimum score for chapter.
        opt_score (float, optional): Optimal score for chapter.
        question ([object]): A list of question for this chapter, see below.

    Question attributes:
        title (str): Question title.
        description (str, optional): A description of the question.
        weight (float, optional): Weight of the question.
        answer (str): Expected answer to the qeustion.
        hint (str, optional): A hint for the question.
    """
    jsonPost = await request.json()
    data = None

    if isinstance(jsonPost, dict):
        data = jsonPost
    else:
        data = json.loads(jsonPost)

    title = data["title"]
    hostname = data["hostname"]
    version = data["version"]
    try:
        session = database.Session()
        resp = (
            session.query(Survey)
            .filter(
                Survey.title == title
                and Survey.hostname == hostname
                and Survey.version == version
            )
            .all()
        )

        if len(resp):
            logging.debug(
                "Survey"
                + title
                + ":"
                + hostname
                + "/"
                + str(version)
                + " already exists."
            )
            return resp[0].id

        survey = Survey(title)

        survey.version = version
        survey.description = data.get("description")
        survey.hostname = hostname
        survey.ip = request.client
        survey.port = data["port"]

        session.add(survey)
        session.commit()

        chapter_sort_no = 0
        for chapterData in data["chapter"]:
            chapter = Chapter(survey.id, chapterData["title"], chapter_sort_no)
            chapter.description = chapterData.get("description")
            chapter.min_score = chapterData.get("min_score")
            chapter.opt_score = chapterData.get("opt_score")
            session.add(chapter)
            session.commit()

            questions = []
            for questionData in chapterData["question"]:
                question = Question(
                    chapter.id,
                    questionData.get("title"),
                    len(questions),
                    questionData.get("weight"),
                    questionData.get("answer"),
                    questionData.get("hint"),
                )
                question.description = questionData.get("description")
                questions.append(question)

            session.add_all(questions)
            session.commit()

            chapter_sort_no += 1
        logging.info("New Survey: " + str(survey.id))
        return survey.id
    finally:
        session.close()


@app.get("/surveys/list")
async def list_surveys(request: Request):
    surveys = []
    try:
        session = database.Session()
        resp = session.query(Survey).all()
        for survey in resp:
            surveys.append(
                {
                    "id": survey.id,
                    "title": survey.title,
                    "version": survey.version,
                    "description": survey.description,
                    "hostname": survey.hostname,
                    "ip": survey.ip,
                    "port": survey.port,
                }
            )
        return surveys
    finally:
        session.close()


@app.post("/software")
async def add_software(request: Request):
    """Endppoint for adding new software information using a POST request.

    When the request is processed successfully, a new software with the provided title
    and URL is created in the database.

    Args:
        request (fastapi.Request): The FastAPI request object.

    Software object attributes:
        title (str): Title of the software.
        url (str): URL of the software.

    Raises:
        HTTPException: Returns status code 500 upon errors.

    Returns:
        int: The ID of the newly added software.
    """
    jsonPost = await request.json()
    data = None

    if isinstance(jsonPost, dict):
        data = jsonPost
    else:
        data = json.loads(jsonPost)

    title = data["title"]
    url = data["url"]
    try:
        session = database.Session()
        software = Software(title, url)

        session.add(software)
        session.commit()

        logging.info("New Software: " + str(software.id))
        return software.id
    finally:
        session.close()


@app.post("/evaluate/{softwareID}/{surveyID}")
def evaluate_software(softwareID: str, surveyID: str):
    """Endpoint for initiating the evaluation of a software.

    If the task is processed successfully, an evaluation task is created or continued
    for the specified software and survey, depending on the status. The status will be
    set to `in_progress`, and the evaluation task will be sent to an external endpoint
    for processing.

    Args:
        softwareID (str): The ID of the software to be evaluated.
        surveyID (str): The survey ID belonging to the evaluation.

    Returns:
        dict: A JSON object with the following attributes:
            job_id (str): The ID of the evaluation job.
            status (str): Status of the evaluation job (e.g. `open`, `in_progress`)

    Raises:
        HTTPException: Returns appropriate HTTP status code upon errors.
    """
    try:
        session = database.Session()

        software = session.query(Software).filter(Software.id == softwareID).first()

        survey = session.query(Survey).filter(Survey.id == surveyID).first()

        job = (
            session.query(Job)
            .filter(Job.software_id == softwareID and Job.survey_id == surveyID)
            .first()
        )

        if job is not None and job.status == Status.error:
            session.delete(job)
            job = None

        if job is None:
            job = Job(softwareID=softwareID, surveyID=surveyID)
            job.status = Status.open
            session.add(job)
            session.commit()
        elif job is not None:
            return {"job_id": job.id, "status": str(job.status)}

        requests.post(
            "http://" + survey.hostname + ":" + str(survey.port) + "/evaluate",
            json={"url": software.url, "id": str(software.id)},
            timeout=5,
        )

        job.status = database.Status.in_progress
        session.commit()

        return {"job_id": job.id, "status": str(job.status)}
    finally:
        session.close()


@app.post("/evaluated/{softwareID}/{surveyID}")
async def evaluate_software(softwareID: str, surveyID: str, request: Request):
    """Endpoint for handling the results of a software evaluation.

    If the request is processed successfully, the status of the evaluation task in the
    database is updated accordingly (either set to `done` for successful completion or
    `error`for an error). This is done to track the completion of the evaluation.

    Args:
        softwareID (str): The ID of the evaluated software.
        surveyID (str): The ID fo the associated survey.
        request (fastapi.Request): The FastAPI request object containing the evaluation
            results.

    Returns:
        None

    Raises:
        HTTPException:
    """
    jsonPost = await request.json()
    data = None

    if isinstance(jsonPost, dict):
        data = jsonPost
    else:
        data = json.loads(jsonPost)

    logging.debug("Evaluated " + softwareID)
    logging.debug(data)

    try:
        session = database.Session()

        job = (
            session.query(Job)
            .filter(Job.software_id == softwareID and Job.survey_id == surveyID)
            .first()
        )
        if data["status"]:
            job.status = Status.done
        else:
            job.status = Status.error

        session.commit()
    finally:
        session.close()


@app.get("/status/{job_id}")
def get_status(job_id: str):
    """Endpoint for retrieving the status of an evaluation job.

    If the request is processed successfully, the current status of the evaluation task
    is retrieved from the database and returned as text. This allows monitoring the
    progress of the task.

    Args:
        job_id (str): _description_

    Returns:
        _type_: _description_
    """
    try:
        session = database.Session()
        return session.query(Job).filter(Job.id == job_id).first().status.name
    finally:
        session.close()


if __name__ == "__main__":
    debugpy.listen(("0.0.0.0", 5678))

    port = os.environ.get("API_PORT")
    port = 5000 if port is None else port

    uvicorn.run(app, host="0.0.0.0", port=int(port))
