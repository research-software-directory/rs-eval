# SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2023 Jonas Rimatzki (UP, GFZ) <rimatzkijo@yahoo.de; rimatzki@uni-potsdam.de; jr@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2023 Universität Potsdam
# SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
#
# SPDX-License-Identifier: EUPL-1.2

import logging, os
import time, threading
import requests, yaml, json


class comm:
    """
    Class for communication with the controller.

    Attributes:
        controller_service_name (str): The name of the controller service.
        controller_port (str): The port of the controller service.
        hostname (str): The hostname of the local machine.
        port (int): The port number of the local service.
        version (str): The version of the yaml syntax.

    TODO:
        - why is there controller_service_name and hostname?

    This class handles:
    - registration during start of container.
    - notifications about successful pass or errors during the evaluation.

    Uses `survey.yaml` to register.
    """

    controller_service_name = None
    controller_port = None
    hostname = None
    port = None
    version = None

    def __init__(self):
        self.controller_service_name = "localhost"
        self.controller_port = "5000"
        self.hostname = "localhost"
        self.port = 5001
        self.version = 1

    def setup(self):
        """
        Setup communication parameters using environment variables.
        """
        self.controller_service_name = os.environ.get("CONTROLLER_SERVICE_NAME")
        self.controller_port = os.environ.get("CONTROLLER_PORT")
        self.hostname = os.environ.get("HOSTNAME")
        self.port = os.environ.get("SERVICE_PORT")
        self.version = os.environ.get("SERVICE_VERSION")

        exc = ""
        if self.controller_service_name is None:
            exc += "$CONTROLLER_SERVICE_NAME not set.\n"
        if self.controller_port is None:
            exc += "$CONTROLLER_PORT not set.\n"
        if self.hostname is None:
            exc += "$HOSTNAME not set.\n"
        if self.port is None:
            exc += "$SERVICE_PORT not set.\n"
        if self.version is None:
            exc += "$SERVICE_VERSION not set.\n"

        if len(exc):
            raise Exception(exc)

    def register(self):
        """
        Registers the local service with the controller.
        """
        for filepath in os.listdir(r"."):
            if filepath.endswith(".yaml"):
                with open(filepath, "r", encoding="UTF-8") as stream:
                    yaml_data = yaml.safe_load(stream)

                    yaml_data["hostname"] = self.hostname
                    yaml_data["port"] = self.port
                    yaml_data["version"] = self.version

                    payload = json.dumps(yaml_data)

                    resp = requests.post(
                        "http://"
                        + self.controller_service_name
                        + ":"
                        + self.controller_port
                        + "/register",
                        json=payload,
                        timeout=5,
                    )

                    logging.info(resp.json())

                    return

    def notify_controller(self, softwareID, status):
        """
        Notifies the controller about the status of a software.

        Args:
            softwareId (str): The ID of the software.
            status (bool): The status of the software.
        """
        requests.post(
            "http://"
            + self.controller_service_name
            + ":"
            + self.controller_port
            + "/evaluated/"
            + softwareID
            + "/"
            + self.hostname,
            json={"status": status},
            timeout=5,
        )


if __name__ == "__main__":
    comm().register()
