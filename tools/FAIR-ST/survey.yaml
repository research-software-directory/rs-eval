# SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2023 Jonas Rimatzki (UP, GFZ) <rimatzkijo@yahoo.de; rimatzki@uni-potsdam.de; jr@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2023 Universität Potsdam
#
# SPDX-License-Identifier: EUPL-1.2

title: FAIR-ST
chapter:
  - title: Findable
    description: The following questions address the aspect of being able to find and uniquely identify the software. For each question, provide a check if the question can be answered with yes.
    min_score:
    opt_score:
    question:
      - title: Open Publication Repository
        weight: 0.2
        answer:
          - There is no information available on where to find the software.
          - Is there an online repository which contains the software?
          - Is there some kind of description available giving further information on the software in this repository (e.g. readme file)?
          - Is there a structured meta data description (e.g. following DataCite) given for software in this repository?
          - Is the repository listed in some overarching meta-repository?
          - Is the repository listed in a meta-repository performing quality checks (e.g. re3data)?
        hint:
          - You should make your software available for users. Repositories like GIT are a great solution for that, allowing you to add a description for your software as well as other metadata.
          - Making your software available in an online repository should be the next big step for you. That way people can find your software much more easily, without any additional work for you.
          - Why not add a readme-file to your repository? That way people can see what your software does, as well as get some additional information for usage!
          - A readme file is much more usable when computers can work with it as well. Consider a structured and machine-readable description format like DataCite!
          - When the repository you use to give people access to your software is listed in some meta-repository users have a much easier time finding your software. Check if your repository is listed in any!
      - title: Versioning
        weight: 0.3
        answer:
          - No software versioning applied
          - Is there some kind of version for the software?
          - Does the versioning provide information on minor/major releases?
          - Is a description of the versioning scheme available?
          - Is there a roadmap giving further information on software releases?
          - Does the versioning scheme allow for automatic tagging by CI/CD processes?
        hint:
          - Show your users that you are working on different things or when you add new features by giving your software version numbers! Maybe follow the usual format of major.minor.patch?
          - Versioning is much more useful for the user if a format like major.minor.patch is used, so they can easily identify the range of changes that happend since your last release.
          - Why not explain your versioning scheme to the user? If you are using major.minor.patch, consider a roadmap for people to know what features they can look out for in the next releases!
          - Describing your versioning scheme is only half of the work. Show your users what you are working on and plan for the future by adding in a roadmap!
          - A CI/CD process can easily automate your releases for you, as well as many other things! It would make a great addition to your roadmap.
      - title: Persistent Identifier
        weight: 0.2
        answer:
          - No PIDs given.
          - Is there a handle/URL given to identify the software?
          - Is the identifier provided with a defined metadata scheme?
          - Is a persistent identifier given?
          - Is a PID given allowing for automated harvesting of metadata information?
          - Is the PID listed in a meta-PID scheme?
        hint:
          - Give your software some kind of PID. This might be as simple as adding the repository URL to your project.
          - If you give more than just the URL, for example a metadata scheme, people can more easily identify what your software is capable of doing and compare it with others.
          - Give your software a persistent identifier like a DOI. That way people can actually find more information on your software and share it in the community.
          - A PID can support automatic harvesting. That way your repository might be able to automatically get updated information, sparing you the work for updating the repository yourself.
          - If your PID is listed in a meta-PID scheme its much more simpler for people to find your software by your PID.
      - title: Rich Metadata
        weight: 0.3
        answer:
          - No metadata given.
          - Is there some metadata information given?
          - Is the metadata information complete?
          - Are updates for the metadata information being provided?
          - Can all metadata information be automatically harvested?
          - Is there an additional procedure for quality assurance (e.g. review)?
        hint:
          - To improve software visibility, consider adding metadata. Even basic metadata helps users understand your software.
          - You're on the right track! Completing metadata details is essential. Users will appreciate comprehensive information about your software.
          - Keeping metadata up-to-date shows your commitment. Users value accurate information about changes and improvements.
          - Enabling automated metadata harvesting ensures consistency. Users can find the latest details without manual updates.
          - Consider implementing quality assurance for metadata. Peer reviews or checks enhance accuracy and reliability.
  - title: Accessible
    description: The following questions address the aspect of being able to access research software. Accessing included the possibility to run the software, which might also be in terms of a web service. However, accessibility does not include the possibility to adjust the code which is rather being captured under the aspect of reusability. For each question, provide a check if the question can be answered with yes.
    min_score:
    opt_score:
    question:
      - title: Access Conditions (legal)
        weight: 0.4
        answer:
          - Not specified.
          - Is there a contact given which to inquire about the right to use the software?
          - Is there a license given describing rights of use?
          - Does the license allow for open use of the software?
          - Is there a way to also obtain support in using the software?
          - Is there a community given, providing the opportunity of support and exchange concerning aspects of using the software?
        hint:
          - Give people a way to contact you, so they can ask for the rights and you can consider allowing that. Or come up with a license to spare yourself that work.
          - Make sure your software's access conditions are clear. Include a well-defined license and a point of contact for inquiries about usage rights.
          - Consider whether you want to offer open use of your software or if you have specific usage restrictions outlined in the license.
          - Providing a way for users to obtain support enhances their experience with your software. Consider offering documentation or resources.
          - Building a community around your software fosters support, collaboration, and exchange of ideas among users.
      - title: Access Options (process)
        weight: 0.3
        answer:
          - There is only one specific form of accessing the software or no option at all.
          - Are sources or executables being provided?
          - Are sources and executables being provided including some documentation on how to install/use the software?
          - Are test cases provided allowing to determine whether installation/execution worked as being expected?
          - In addition, are checks provided making sure the software works correctly?
          - Is there a software service being given?
        hint:
          - Ensure your software is technically accessible. Offer installation guidance, scripts, and options like makefiles or packages for user convenience.
          - Providing source code allows users to build your software using package managers or automake tools, enhancing flexibility.
          - Including documentation alongside sources or executables can guide users through the installation and usage process.
          - Offering checks or validation mechanisms can help users ensure that the software is working correctly and producing accurate results.
          - If applicable, mention whether your software offers any form of software-as-a-service (SaaS) for users' convenience.
      - title: Technical Accessibility (run/start)
        weight: 0.3
        answer:
          - No information given.
          - Is there a “How to install”/readme file provided?
          - Are installation scripts being provided?
          - Is there a makefile or manual package (e.g. python modules) being provided allowing for (semi-)automatized installation?
          - Are sources being provided such that a package manager or automake can be used?
          - Is there a complete package available (e.g. container, app package)?
        hint:
          - Providing a "How to install" or readme file can significantly aid users in getting started with your software.
          - Installation scripts can streamline the setup process and help users quickly deploy your software.
          - Consider including a makefile or manual package (e.g., Python modules) to enable (semi-)automated installation methods.
          - Providing source code lets users utilize package managers or automake tools for efficient software installation.
          - Offering a complete package, like a container or app package, simplifies installation for users.
  - title: Interoperable
    description: The following questions address the aspect of being interoperable, i.e. the possibilities of being able to integrate the software into one’s own software framework or execution pipelines. For each question, provide a check if the question can be answered with yes.
    min_score:
    opt_score:
    question:
      - title: Input/Output Formats/API
        weight: 0.4
        answer:
          - Not specified.
          - Is there some description of Input and output formats?
          - Does the software build on standard formats for input and output?
          - Are there additional options provided for varying input/output formats?
          - Does the software build on accepted community standards for input/output data?
          - Does the software in addition provide further tools for processing input/output data?
        hint:
          - Describe input/output formats for seamless integration with other tools.
          - Utilizing standard formats ensures compatibility with various software systems.
          - Providing options for varying input/output formats accommodates diverse integration needs.
          - Aligning with community standards enhances data exchange with other software.
          - Offering tools for processing input/output data enhances the software's utility.
      - title: Qualified Reference
        weight: 0.6
        answer:
          - No information given.
          - Is there a way to use the software with defined input/output data?
          - Are there further parameters to adjust the way the software is working?
          - Is there some way of logging what is done during execution?
          - Are there APIs being provided to integrate the software into one’s own framework?
          - Is there a way to integrate the software into open workflows?
        hint:
          - Document usage with defined input/output data for clear integration guidelines.
          - Offering adjustable parameters allows users to customize the software's behavior.
          - Implementing logging during execution aids in tracking progress and debugging.
          - APIs for integration into external frameworks enhance software versatility.
          - Enabling integration into open workflows promotes collaboration and adoption.
  - title: Reusable
    min_score:
    opt_score:
    description: The following questions address the aspect of being reusable. In addition to being accessible, i.e., executable, reusability includes the possibility to actually change/adapt the code. For each question, provide a check if the question can be answered with yes.
    question:
      - title: Reusability Conditions
        weight: 0.5
        answer:
          - Not clear.
          - Is there a custom license allowing reuse of the software?
          - Is there a FOSS license including that license dependencies are at least being checked manually?
          - Is there an appropriate license for different file types (code, text, images, etc.) following the REUSE specification?
          - Is there a process available for automatically checking the REUSE specification?
          - Is there a process available such that all dependencies are automatically controlled?
        hint:
          - Clear reusability conditions make your software more inviting for others to build upon.
          - Utilize a custom license to explicitly allow reuse of your software's code.
          - Applying a FOSS license empowers users to confidently adapt and modify your software.
          - Align with the REUSE specification by choosing suitable licenses for different file types.
          - Implement automated checks to ensure compliance with the REUSE specification.
      - title: Qualified References
        weight: 0.5
        answer:
          - No references provided.
          - Are there references to other publications in the repository?
          - Are the references provided in a human-readable format including DOIs?
          - Does the list of references follow a defined bibliography style?
          - Is there a list of references in a reference manager format (e.g., bibtex)?
          - Are references listed in a machine-readable file format designated for software metadata (e.g., CITATION.cff, CodeMeta)?
        hint:
          - Including qualified references enhances the credibility of your software's scientific foundation.
          - Consider referencing relevant publications within your software repository.
          - Ensure references are provided in a readable format, including DOIs for easy access.
          - Structuring references according to a defined bibliography style improves clarity.
          - Offering references in reference manager formats like bibtex facilitates citation integration.
  - title: Scientific basis
    description: The following questions address the aspect of the software being scientifically well grounded. While domain specific scientific requirements have to be assessed as part of a scientific peer-review process, certain generic aspect of good scientific practice can be assessed for all research software.  For each question, provide a check if the question can be answered with yes.
    min_score:
    opt_score:
    question:
      - title: Community Standards
        weight: 0.3
        answer:
          - No information provided.
          - Connection to known scientific standards drawn?
          - Software follows standards of the relevant scientific community?
          - Software complies with relevant scientific standards of the field?
          - Indication on addressing further evolution of community standards?
          - Closed feedback-loop established to adopt evolving community standards?
        hint:
          - Improve the connection to known scientific standards for credibility.
          - Ensure alignment with standards of the relevant scientific community.
          - Comply with scientific standards of the field to enhance acceptance.
          - Consider addressing future evolution of community standards.
          - Establish a closed feedback-loop for adopting evolving community standards.
      - title: Team Expertise
        weight: 0.3
        answer:
          - No information provided.
          - Clear that at least one domain expert is part of the software development team?
          - Software development team comprises expertise in several relevant domains?
          - Software development team comprises expertise in all relevant domains?
          - Established interdisciplinary team of software developers?
          - Established and coordinated community of software developers working on the software?
        hint:
          - Ensure domain expertise in the software development team for effective collaboration.
          - Consider having expertise from multiple relevant domains for well-rounded development.
          - Maximize expertise coverage across all relevant domains for comprehensive software development.
          - Build an interdisciplinary team of software developers for a holistic approach.
          - Foster an established and coordinated community of software developers for collective growth.
      - title: Scientific Embedding
        weight: 0.4
        answer:
          - No information provided.
          - At least one documented scientific use case?
          - Broader scientific context documented, including multiple examples?
          - Software development loosely connected to a scientific initiative?
          - Software development part of a larger scientific initiative?
          - Software development part of a larger scientific initiative with dedicated software development processes?
        hint:
          - Document at least one scientific use case to showcase practical application.
          - Provide a broader scientific context with multiple examples for a comprehensive view.
          - Establish a connection between software development and scientific initiatives.
          - Integrate software development into a larger scientific initiative for synergy.
          - Align software development within a larger scientific initiative and implement dedicated software development processes.
  - title: Technical basis
    description: The following questions address aspects of professional software development leading to sustainable, high quality research software. For each question, provide a check if the question can be answered with yes.
    min_score:
    opt_score:
    question:
      - title: Project Management
        weight: 0.2
        answer:
          - No information on project management and code history being provided.
          - Is there some kind of version control being used?
          - Is there a version control system in place being part of a code management platform (e.g. GitHub, GitLab) and an associated ticket system?
          - Is there a defined process for ticket resolving, code review by other developer, and merge requests being established?
          - Is there a release process with at least manual curation being established?
          - Is there a release process with automated changelog generation, testing, and product provisioning being established?
        hint:
          - To enhance project management, start by providing information on code history and management practices.
          - Implement version control (e.g., Git) to track changes and facilitate collaboration.
          - Consider utilizing a code management platform like GitHub or GitLab that integrates version control and a ticket system for efficient development workflows.
          - Establish a clear process for handling tickets, conducting code reviews, and managing merge requests to ensure code quality and collaboration.
          - For effective software releases, create a defined release process that involves manual curation and quality checks, and explore automation for optimizing the process.
      - title: Repository Structure
        weight: 0.2
        answer:
          - No information given.
          - Are all files given in a single folder?
          - Is the repository structured, allowing contributors to follow their standards?
          - Is there a CONTRIBUTORS.md file, a defined structure, and a documented onboarding process?
          - Is there an automated process for generating the repository structure and identifying deviations?
          - Is a repository structure enforced following community standards?
        hint:
          - Organize the repository for clarity and maintainability.
          - Allow contributors to follow their coding standards, but define an overall structure.
          - Include a CONTRIBUTORS.md file, documented structure, and an onboarding process.
          - Consider automating the repository structure generation and tracking deviations.
          - Aim to align with community standards for repository structure.
      - title: Code Structure
        weight: 0.2
        answer:
          - No information given.
          - Can developers use their coding style?
          - Are there general coding recommendations while allowing developers to follow their style?
          - Is there harmonization of code style following common standards, including meaningful naming?
          - Is code style checked during merge requests?
          - Is code style enforced through review processes or auto-formatting?
        hint:
          - Encourage harmonization of code style for readability and collaboration.
          - Provide general coding recommendations while allowing flexibility.
          - Enforce common coding standards for consistency, including meaningful naming.
          - Implement code style checks during merge requests.
          - Use review processes or automated tools to ensure code style and formatting.
      - title: Reproducibility (Code)
        weight: 0.2
        answer:
          - No tests or duplicated code.
          - Does the code follow a modular structure allowing component reusability?
          - Are clear system requirements documented with min/max versions, while enforcing version pinning and modularity manually?
          - Is a package manager used for dependency pinning and testing?
          - Is test coverage measured, even if tests are voluntary?
          - Is there automated testing for different environments, minimum test coverage requirements, and containerized packages?
        hint:
          - Design code with a modular structure for component reusability.
          - Document clear system requirements and enforce version pinning and modularity.
          - Utilize a package manager for dependencies and endorse testing.
          - Aim for test coverage, even if tests are written voluntarily.
          - Implement automated testing for various environments and provide containerized packages.
      - title: Security
        weight: 0.2
        answer:
          - No security concepts given.
          - Are sporadic updates and dependency checks prioritized?
          - Is there a systematic assessment of dependencies and documentation of the software stack?
          - Is deployment within a CI/CD framework provided for different environments, including security checks?
          - Is there a process for monitoring dependency updates, including reporting?
          - Is there regular and automated security monitoring, with updates requiring passing security checks?
        hint:
          - Prioritize sporadic updates and dependency checks for software security.
          - Systematically assess and document software stack dependencies.
          - Deploy within a CI/CD framework for consistent security checks.
          - Monitor dependency updates and establish a reporting mechanism.
          - Implement regular automated security monitoring and updates.
