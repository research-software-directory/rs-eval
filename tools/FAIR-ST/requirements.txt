fastapi==0.100.0
uvicorn==0.22.0

validators==0.20.0
pyyaml==6.0
requests==2.28.1

debugpy