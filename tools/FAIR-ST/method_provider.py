# SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import os, subprocess, threading
import json, yaml, validators, requests
from datetime import datetime
from urllib.parse import urlparse
from fastapi import FastAPI, Request, HTTPException, Response
from fastapi.responses import JSONResponse
import uvicorn
import toml
import debugpy

from comm import comm

logging.basicConfig(level=logging.DEBUG)

commInst = comm()
# comment for testing without the env variables.
commInst.setup()
commInst.register()

app = FastAPI()


def git_api(git_url: str, submodule: str):
    """
    Fetches information about a submodule from a GitHub repository using its API.

    Args:
        git_url (str): The URL of the GitHub repository.
        submodule (str): The name of the submodule to retrieve information about.

    Returns:
        dict: A dictionary containing information about the submodule fetched from the GitHub API.

    Raises:
        TypeError: If the provided git_url is not a valid URL.
        ConnectionError: If there is an issue connecting to the GitHub API or retrieving data.
    """
    if not validators.url(git_url):
        raise TypeError(git_url, "No URL!")

    # Parse the GitHub URL
    parsed_url = urlparse(git_url)

    # Extract the location
    domain = parsed_url.netloc

    # Extract the username and repository name
    path_parts = parsed_url.path.split("/")
    username = path_parts[1]
    repo_name = path_parts[2].strip(".git")

    # Construct the API URL
    api_url = f"https://api.{domain}/repos/{username}/{repo_name}/{submodule}"

    response = requests.get(api_url)
    if response.status_code != 200:
        raise ConnectionError(
            api_url, response.status_code, response.reason, response.text
        )
    return response.json()


def rate(chapter: str, question: str, hermes_dict: dict, git_url: str):
    """
    Rates the findability, accessibility, interoperability, reusability, scientific basis, and technical basis
    of a resource based on HERMES and GitHub data.

    Args:
        chapter (str): The chapter/category under which the question falls.
        question (str): The specific question within the chapter.
        hermes_dict (dict): Dictionary containing information extracted by HERMES.
        git_url (str): The URL of the GitHub repository associated with the resource.

    Returns:
        list: A list containing boolean values indicating the assessment result for various criteria:
            - [0]: Findable (Open Publication Repostiory)
            - [0]: Findable (Open Publication Repository)
            - [1]: Findable (Access Conditions)
            - [2]: Findable (Versioning), Accessible (Persistent Identifier), Reusable (Reusability Conditions)
            - [3]: Accessible (Rights)
            - [4]: Accessible (Issue Tracking)
            - [5]: Accessible (Community Engagement)
            - [6]: Interoperable (Input/Output Formats/API), Reusable (Qualified References), Scientific basis (Community Standards)
            - [7]: Interoperable (Qualified Reference), Scientific basis (Team Expertise)
            - [8]: Interoperable (Technical Accessibility)
            - [9]: Scientific basis (Scientific Embedding)
            - [10]: Technical basis (Project Management)
            - [11]: Technical basis (Repository Structure)
            - [12]: Technical basis (Code Structure)
            - [13]: Technical basis (Reproducibility)
            - [14]: Technical basis (Security)

    Raises:
        ConnectionError: If there is an issue connecting to the GitHub API or retrieving data.
    """
    rv = [None, None, None, None, None, None]

    if chapter == "Findable":
        if question == "Open Publication Repository":
            if requests.get(git_url).status_code != 200:
                rv[1] = True

            git_community_dict = git_api(git_url, "community/profile")

            if (
                git_community_dict["health_percentage"] == 100
                or git_community_dict["files"].get("readme", None) != None
            ):
                rv[2] = True
        elif question == "Versioning":
            # Git-API: Milestones, Releases, Commits, etc.
            pass
        elif question == "Persistent Identifier":
            pass
        elif question == "Rich Metadata":
            pass
    elif chapter == "Accessible":
        if question == "Access Conditions (legal)":
            license = None

            # No license found by HERMES
            if hermes_dict.get("license", None) == None:
                rv = [False for _ in rv]
                authors = hermes_dict.get("author", None)
                for author in authors:
                    # And author can be contacted via email or has an orcid
                    if validators.email(author.get("email")) or (
                        validators.url(author.get("@id"))
                        and author.get("@id").contains("orcid.org")
                    ):
                        rv[1] = True
                if not rv[1]:
                    rv[0] = True
            else:
                # Download license list from GitHub
                response = requests.get("https://api.github.com/licenses")
                if response.status_code != 200:
                    raise ConnectionError(
                        response.url,
                        response.status_code,
                        response.reason,
                        response.text,
                    )
                github_licenses = (
                    response.json()
                )  # Parse JSON response into a dictionary

                hermes_licenses = os.path.basename(hermes_dict["license"])
                for github_license in github_licenses:
                    if github_license["spdx_id"] in hermes_licenses:
                        response = requests.get(github_license["url"])
                        if response.status_code != 200:
                            raise ConnectionError(
                                requests.get(github_license["url"]),
                                response.status_code,
                                response.reason,
                                response.text,
                            )
                        license = response.json()
                        # TODO Can there be multiple licenses in hermes?
                        break
                rv[2] = True

                # TODO What happens here?
                rights = [
                    "commercial-use",
                    "modifications",
                    "distribution",
                    "patent-use",
                    "private-use",
                ]
                if all(element in license["permissions"] for element in rights):
                    rv[3] = True

                # Check activity
                # Set prop to true if there was issue activity in the last 90 days
                git_issue_dict = git_api(git_url, "issues?sort=created&per_page=100")
                # TODO why should there be more than 10 issues?
                if len(git_issue_dict) >= 10:
                    for issue_idx in range(len(git_issue_dict)):
                        update_date = datetime.strptime(
                            git_issue_dict[issue_idx]["updated_at"].split("T")[0],
                            "%Y-%m-%d",
                        )
                        create_date = datetime.strptime(
                            git_issue_dict[issue_idx]["created_at"].split("T")[0],
                            "%Y-%m-%d",
                        )
                        if 0 < (update_date - create_date).days < 90:
                            rv[4] = True
                            break

                git_watcher_count = len(git_api(git_url, "subscribers"))
                git_starrer_count = len(git_api(git_url, "stargazers"))

                if (git_watcher_count + git_starrer_count) >= 50:
                    rv[5] = True
        elif question == "Technical Accessibility (run/start)":
            # Git Community Metrics
            git_community_dict = git_api(git_url, "community/profile")

            if (
                git_community_dict["health_percentage"] == 100
                or git_community_dict["files"].get("readme", None) != None
            ):
                rv[0] = False
                rv[1] = True
    elif chapter == "Interoperable":
        if question == "Input/Output Formats/API":
            pass
        elif question == "Qualified Reference":
            pass
    elif chapter == "Reusable":
        if question == "Reusability Conditions":
            pass
        elif question == "Qualified References":
            pass
    elif chapter == "Scientific basis":
        if question == "Community Standards":
            pass
        elif question == "Team Expertise":
            pass
        elif question == "Scientific Embedding":
            pass
    elif chapter == "Technical basis":
        if question == "Project Management":
            pass
        elif question == "Repository Structure":
            pass
        elif question == "Code Structure":
            pass
        elif question == "Reproducibility (Code)":
            pass
        elif question == "Security":
            pass
    return rv


def create_eval_dict(softwareID, url):
    """
    Creates an evaluation dictionary based on survey questions and HERMES data.

    Args:
        softwareID (str): Identifier for the software.
        url (str): The URL of the software.

    Returns:
        dict: A dictionary containing evaluation results for each chapter and question.

    Raises:
        FileNotFoundError: If survey.yaml or hermes.json is missing.
    """
    survey_path = "/app/survey.yaml"
    if os.path.exists(survey_path):
        with open(survey_path, "r", encoding="UTF-8") as stream:
            survey_dict = yaml.safe_load(stream)
    else:
        raise FileNotFoundError("Missing survey.yaml!")

    if survey_dict == None:
        raise FileNotFoundError("No survey.yaml found.")

    eval_dict = dict()
    hermes_path = "/software/" + softwareID + "/.hermes/process/hermes.json"

    if os.path.exists(hermes_path):
        with open(hermes_path, "r") as json_file:
            hermes_dict = json.load(json_file)

            for chapter in survey_dict["chapter"]:
                chapter_dict = dict()
                for question in chapter["question"]:
                    chapter_dict[question["title"]] = rate(
                        chapter["title"], question["title"], hermes_dict, url
                    )

                eval_dict[chapter["title"]] = chapter_dict

            return eval_dict
    else:
        raise FileNotFoundError("Missing hermes.json!")


def git(*args):
    """
    Runs a Git command with the given arguments.

    Args:
        *args: Variable number of arguments representing the Git command and its options.

    Returns:
        int: The return code of the executed Git command.

    Raises:
        CalledProcessError: If the Git command fails.
    """
    return subprocess.check_call(["git"] + list(args))


@app.post("/evaluate")
async def evaluate_software(request: Request):
    """Endpoint for initiating the evaluation of software.

    It the requst is processed successfully, a new thread is started to evaluate the
    software provided by the URL.

    Args:
        request (fastapi.Request): The FastAPI request object.

    Returns:
        JSONResponse: The JSON response indicating the status of the evaluation
            initiation. Upon success, statuscode is 201, otherwise 500 including details
            on the error.

    Raises:
        HTTPException: If the JSON payload does not contain the required fields, status
            code 500 is returned including the Error message *Payload didnt contain the
            relevant data*.
    """
    start_thread = True
    try:
        jsonPost = await request.json()
        data = None

        if isinstance(jsonPost, dict):
            data = jsonPost
        else:
            data = json.loads(jsonPost)

        softwareID = str(data["id"])
        url = data["url"]

        return Response(status_code=201)
    except:
        start_thread = False
        return JSONResponse(
            status_code=500, detail="Payload didnt contain the relevant data."
        )
    finally:
        if start_thread:
            threading.Thread(target=evaluate, args=[url, softwareID]).start()


def evaluate(url, softwareID):
    """
    Evaluates a software repository located at the given URL using HERMES.

    Args:
        url (str): The URL of the software repository.
        softwareID (str): Identifier for the software.

    Raises:
        FileNotFoundError: If required files (surve.yaml or hermes.json) are missing.
        CalledProcessError: If a Git or HERMES command fails.
    """
    if validators.url(url):
        try:
            if not os.path.exists("/software/" + softwareID):
                logging.debug("Starting git clone")
                git("clone", url, "/software/" + softwareID)

            logging.debug("Starting HERMES")
            os.chdir("/software/" + softwareID)

            logging.debug("Generating hermes.toml")
            hermes_config = {
                "harvest": {"sources": ["cff", "git"]},
                "harvest.cff": {"enable_validation": False},
            }
            with open("hermes.toml", "w") as hermes_toml:
                toml.dump(hermes_config, hermes_toml)

            subprocess.check_output("hermes", shell=True)
            subprocess.check_output("hermes harvest", shell=True)
            subprocess.check_output("hermes process", shell=True)
            os.chdir("/app")

            eval_dict = create_eval_dict(softwareID, url)

            output_file = "/software/" + softwareID + ".json"
            with open(output_file, "w") as json_file:
                json.dump(eval_dict, json_file, indent=4)

            commInst.notify_controller(softwareID, True)
        except:
            commInst.notify_controller(softwareID, False)
    else:
        commInst.notify_controller(softwareID, True)


@app.get("/rating/{softwareID}")
def get_rating(softwareID: str):
    """Endpoint for retrieving the rating information for a specific software.

    Args:
        softwareID (str): The software ID.

    Returns:
        JSONResponse: The JSON response containing the rating information for the
            specified software.

    Raises:
        HTTPException: If the evaluation is not yet complete or an error occurs, status
        code 102 is returned, and the error message is *Bewertung läuft noch.*.
    """
    try:
        with open("/software/" + softwareID + ".json", "r") as file:
            data = json.load(file)
            return JSONResponse(content=data)
    except:
        return JSONResponse(status_code=102, detail="Evaluation still running.")


if __name__ == "__main__":
    debugpy.listen(("0.0.0.0", 5678))
    uvicorn.run(app, host="0.0.0.0", port=int(commInst.port))
