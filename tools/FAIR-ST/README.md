# FAIR-ST Implementation

This document contains relevant information from the thesis about this module.

## HERMES Integration

Für den Anwendungsfall als Analyse-Tool
sind lediglich der harvest- und process-Schritt von Interesse. Die Ausführung
von HERMES erfolgt über die Kommandozeile, sodass die Informationen in verborge-
nen Dateien in der Datenstruktur des Analyse-Tools vorliegen. Im Rahmen der Qua-
litätsanalyse werden diese Informationen entsprechend ausgewertet und kombiniert,
um eine Bewertung vorzunehmen.

## Evaluation

Die Bewertung für jede Frage jeder Kategorie des Fragebogens erfolgt einzeln und
wird in Form eines zweidimensionalen Dictionaries. Dieses Dictionary wird auf An-
frage durch den Controller in Form eines JSON-Datei zurückgegeben. Dabei sind die
einträge koherent zum Aufbau des Fragebogens gegliedert. Das Ergebnis einer Frage
ist ein Array aus Boolean oder None. Die Position der Werte entspricht dabei den
einzelnen Aussagen, während der Wert das Ergebnis der automatischen Auswertung
ist. Dabei steht ein True für eine positive Antwort auf die Frage, ein False für eine
negative Antwort und None für eine fehlende Beurteilung.

Die fehlenden Beurteilungen können diverse Ursachen haben. So ist nicht jede Fra-
ge durch eine automatische Analyse zu beantworten, wie bereits in 4.1 ausgeführt.
Andererseits bedeutet ein Mangel an Informationen nicht zwangsläufig eine negative
Antwort, sondern unter Umständen kann keine genaue Aussage möglich sein. Dies ist
insbesondere der Fall, wenn ein Teil der Dokumentation und Metadaten des Projektes
noch ausgearbeitet werden muss oder nicht in maschinenlesbarer Form vorliegt.