<!--
SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.1.1](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/v0.1.0...v0.1.1) (2024-03-26)


### Features

* achieve reuse compliance ([ae7619e](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/ae7619ea95ba4dc67cf3e6dbfc2aec6ce3723101))
* add black linter ([400ede4](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/400ede44ad69fb46f09b4f4a9e1342881ee51b72))
* add CI job for reuse linter ([c71323d](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/c71323d52a319baf7ab3b73687984dce9a1c933c))
* add poetry support ([a886234](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/a886234cc6d018894bfebb9b129c959192425e7c))
* add support for conventional changelog ([1fbc978](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/1fbc978fe1e46994424cf4c60f198758ff9063d4))
* add updater for version in pyproject.toml ([8a06800](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/8a06800c0bfa3392baa6d32cbd337732f7e1b449))
* adds endpoint for listing surveys ([1a66eaa](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/1a66eaa37ab16362bef0ddf9318fab243ee08287))
* adds example evaluation workflow ([e036d17](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/e036d17e9c5833192289a6b58d2b0d3e837e4b94))


### Bug Fixes

* create missing hermes.toml ([1d2666a](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/1d2666a67e38775f2545ed8520bc109a2b6bb5f5))
* look in correct directory for existing software ([199703a](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/199703ada04952a139ee909be04a203528f57db9))
* return HTTP Response instead of JSONResponse ([de8b796](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/de8b796408a20285f116086971555bc96512d324))
* return status string instead of int ([e2fcf59](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/e2fcf592ace3cef454f11e19edcd7a575fa8a360))
* strip .git from repository name ([2934702](https://codebase.helmholtz.cloud/research-software-directory/rs-eval/-/commit/2934702329a32a508cee0d8a205a84477250206f))

## 0.1.0 (2024-03-01)

Initial release with original files from [rimatzki/rs-eval](https://gitup.uni-potsdam.de/rimatzki/rs-eval).
