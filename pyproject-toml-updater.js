// SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
// SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
//
// SPDX-License-Identifier: EUPL-1.2

// see https://github.com/conventional-changelog/standard-version?tab=readme-ov-file#custom-updaters

// This method is used to read the version from the provided file contents.
// The return value is expected to be a semantic version string.
module.exports.readVersion = function (contents) {
    const toml = require('toml');
    var data = toml.parse(contents);
    return data.tool.poetry.version;
}


// This method is used to write the version to the provided contents.
// The return value will be written directly (overwrite) to the provided file.
module.exports.writeVersion = function (contents, version) {
    const regex = /^version = ".+"$/m;
    const new_toml = contents.replace(regex, 'version = "' + version + '"');
    console.dir(new_toml);
    return new_toml;
}
